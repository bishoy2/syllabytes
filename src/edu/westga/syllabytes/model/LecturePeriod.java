package edu.westga.syllabytes.model;

import java.time.LocalDate;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 * Class corresponding to ICalendar VEVENT objects.
 * @author Blake Ishoy
 * @version 1
 */
public class LecturePeriod implements Comparable<Object> {
	
	private StringProperty titleProperty;
	private StringProperty descriptionProperty;
	private ObjectProperty<LocalDate> dateProperty;
	
	/**
	 * Initializes a new LecturePeriod.
	 * @param title Title of lecture
	 * @param description The description of the lecture
	 * @param date The date of the lecture
	 */
	public LecturePeriod(String title, String description, LocalDate date) {
		this.titleProperty = new SimpleStringProperty(title);
		this.descriptionProperty = new SimpleStringProperty(description);
		this.dateProperty = new SimpleObjectProperty<LocalDate>(date);
	}
	
	/**
	 * Gets the title of the LecturePeriod.
	 * @return The name of the LecturePeriod.
	 */
	public StringProperty getTitle() {
		return this.titleProperty;
	}
	
	/**
	 * Gets the description of the LecturePeriod.
	 * @return The description of the LecturePeriod.
	 */
	public StringProperty getDescription() {
		return this.descriptionProperty;
	}
	
	/**
	 * Gets the date of the LecturePeriod.
	 * @return The date of the LecturePeriod.
	 */
	public ObjectProperty<LocalDate> getDate() {
		return this.dateProperty;
	}
	
	/**
	 * Sets the localDate property.
	 * @param localDate the value to be set.
	 */
	public void setDate(LocalDate localDate) {
		this.dateProperty.set(localDate);
	}
	
	/**
	 * Compares 2 dates.
	 * @param lecturePeriod the lecture period being compared.
	 * @return the comparison of the dates
	 */
	public int compareLecturePeriod(LecturePeriod lecturePeriod) {
		return this.dateProperty.get().compareTo(lecturePeriod.dateProperty.get());
	}
	
	/**
	 * Placeholder for compare method above.
	 * @param object Object being compared.
	 * @return the result of compareLecturePeriod
	 */
	public int compareTo(Object object) {
		return this.compareLecturePeriod((LecturePeriod) object);
	}
}
