package edu.westga.syllabytes.model.tests;

import static org.junit.Assert.*;

import java.time.DateTimeException;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import edu.westga.syllabytes.model.CourseSchedule;
import edu.westga.syllabytes.model.LecturePeriod;

/**
 * Test class for the updateAllDates() method in courseSchedule
 * @author Blake Ishoy
 * @version 1
 */
public class WhenAdjustingSchedules {
	
	private CourseSchedule courseSchedule;
	
	/**
	 * Initializes test data member.
	 */
	@Before
	public void setup() {
		this.courseSchedule = new CourseSchedule();
	}
	
	/**
	 * Tests to see if an empty courseSchedule returns null when the startDate is queried.
	 */
	@Test
	public void emptyListShouldReturnNull() {
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.startDateProperty(), null);
	}
	
	/**
	 * Tests to see if an empty courseSchedule returns null when the startDate is queried.
	 */
	@Test
	public void emptyListWithPeriodAddedThenRemovedShouldReturnNull() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 1)));
		this.courseSchedule.clear();
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.startDateProperty(), null);
	}
	
	/**
	 * If a null LecturePeriod is passed to the courseSchedule, then the startDateProperty should be null.
	 */
	@Test
	public void courseScheduleWithOneNullLecturePeriodShouldReturnNull() {
		this.courseSchedule.add(null);
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.startDateProperty(), null);
	}
	
	/**
	 * When one LecturePeriod is added and updateAllDates is called, the LecturePeriod
	 * should be the same as the parameter passed to updateAllDates.
	 */
	@Test
	public void courseScheduleWithOnePeriodShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 1)));
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2016, 8, 1));
	}
	
	/**
	 * When two out of chronological order LecturePeriods are added and updateAllDates is called, the LecturePeriods
	 * should be in order from the startDate passed.
	 */
	@Test
	public void courseScheduleWithTwoPeriodsOutOfChronologicalOrderShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 1)));
		this.courseSchedule.add(new LecturePeriod("Test2", "Testing", LocalDate.of(2015, 5, 1)));
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setTuesdayProperty(true);
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.get(1).getDate().getValue(), LocalDate.of(2016, 8, 2));
	}
	
	/**
	 * When two chronologically ordered LecturePeriods are added and updateAllDates is called, the LecturePeriods
	 * should be in order from the startDate passed.
	 */
	@Test
	public void courseScheduleWithTwoPeriodsInChronologicalOrderShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 1)));
		this.courseSchedule.add(new LecturePeriod("Test2", "Testing", LocalDate.of(2016, 7, 2)));
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setTuesdayProperty(true);
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.get(1).getDate().getValue(), LocalDate.of(2016, 8, 2));
	}
	
	/**
	 * When two identical LecturePeriods are added and updateAllDates is called, the LecturePeriods
	 * should be in order from the startDate passed.
	 */
	@Test
	public void courseScheduleWithTwoIdenticalPeriodsShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 1)));
		this.courseSchedule.add(new LecturePeriod("Test2", "Testing", LocalDate.of(2016, 7, 1)));
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setTuesdayProperty(true);
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.get(1).getDate().getValue(), LocalDate.of(2016, 8, 2));
	}
	
	/**
	 * When 3 identical LecturePeriods are added and updateAllDates is called, the LecturePeriods
	 * should be in order from the startDate passed.
	 */
	@Test
	public void courseScheduleWithThreeIdenticalPeriodsShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 1)));
		this.courseSchedule.add(new LecturePeriod("Test2", "Testing", LocalDate.of(2016, 7, 1)));
		this.courseSchedule.add(new LecturePeriod("Test3", "Testing", LocalDate.of(2016, 7, 1)));
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setTuesdayProperty(true);
		this.courseSchedule.setWednesdayProperty(true);
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.get(1).getDate().getValue(), LocalDate.of(2016, 8, 2));
		assertEquals(this.courseSchedule.get(2).getDate().getValue(), LocalDate.of(2016, 8, 3));
	}
	
	/**
	 * When 3 chronologically ordered LecturePeriods are added and updateAllDates is called, the LecturePeriods
	 * should be in order from the startDate passed.
	 */
	@Test
	public void courseScheduleWithThreePeriodsInChronologicalOrderShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 1)));
		this.courseSchedule.add(new LecturePeriod("Test2", "Testing", LocalDate.of(2016, 7, 2)));
		this.courseSchedule.add(new LecturePeriod("Test2", "Testing", LocalDate.of(2016, 7, 3)));
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setTuesdayProperty(true);
		this.courseSchedule.setWednesdayProperty(true);
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.get(1).getDate().getValue(), LocalDate.of(2016, 8, 2));
		assertEquals(this.courseSchedule.get(2).getDate().getValue(), LocalDate.of(2016, 8, 3));
	}
	
	/**
	 * When 3 not chronologically ordered LecturePeriods are added and updateAllDates is called, the LecturePeriods
	 * should be in order from the startDate passed.
	 */
	@Test
	public void courseScheduleWithThreePeriodsOutOfChronologicalOrderShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 3)));
		this.courseSchedule.add(new LecturePeriod("Test2", "Testing", LocalDate.of(2016, 7, 2)));
		this.courseSchedule.add(new LecturePeriod("Test2", "Testing", LocalDate.of(2016, 7, 1)));
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setTuesdayProperty(true);
		this.courseSchedule.setWednesdayProperty(true);
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.get(1).getDate().getValue(), LocalDate.of(2016, 8, 2));
		assertEquals(this.courseSchedule.get(2).getDate().getValue(), LocalDate.of(2016, 8, 3));
	}
	
	/**
	 * When invalid LocalDate is passed to updateAllDates, DateTimeException is thrown.
	 */
	@Test(expected = DateTimeException.class)
	public void passingInvalidLocalDateToUpdateAllDatesShouldThrowDateTimeException() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 3)));
		this.courseSchedule.updateAllDates(LocalDate.of(0, 0, 0));
	}
	
	/**
	 * When invalid LocalDates are passed to courseSchedule, then updateAllDates is called, DateTimeException is thrown.
	 */
	@Test(expected = DateTimeException.class)
	public void passingInvalidLocalDateToCourseScheduleUpdateAllDatesShouldThrowDateTimeException() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(0, 0, 0)));
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 3));
	}
	
	/**
	 * Tests what happens when a null LocalDate is passed to updateAllDates.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void passingNullStartDateIntoUpdateAllDatesShouldReturnThrowIllegalArgException() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 3)));
		this.courseSchedule.updateAllDates(null);
	}
	
	/**
	 * Tests how the earliest date 1/1/1 behaves.
	 */
	@Test
	public void earliestStartDateBehavesNormally() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 3)));
		this.courseSchedule.updateAllDates(LocalDate.of(1, 1, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(1, 1, 1));
	}
	
	/**
	 * Tests what happens when the lecturePeriod's date is the same as the updateAllDates parameter.
	 */
	@Test
	public void sameDateAsLecturePeriodShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 3)));
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 7, 3));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2016, 7, 3));
	}
	
	/**
	 * Tests what happens when the lecturePeriod's date is before the updateAllDates parameter.
	 */
	@Test
	public void earlierDateThanLecturePeriodShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 3)));
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 7, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2016, 7, 1));
	}
	
	/**
	 * Tests what happens when the lecturePeriod's date is after the updateAllDates parameter.
	 */
	@Test
	public void laterDateThanLecturePeriodShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 3)));
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 7, 5));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2016, 7, 5));
	}
	
	/**
	 * Tests what happens when the date flips over the end of a month.
	 */
	@Test
	public void flippingOverEndOfMonthShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 7, 31)));
		this.courseSchedule.updateAllDates(LocalDate.of(2016, 8, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2016, 8, 1));
	}
	
	/**
	 * Tests what happens when the date flips over the end of a year.
	 */
	@Test
	public void flippingOverEndOfYearShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 12, 31)));
		this.courseSchedule.updateAllDates(LocalDate.of(2017, 1, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2017, 1, 1));
	}
	
	/**
	 * Tests what happens when lecturePeriods in a schedule are very spaced out.
	 */
	@Test
	public void verySpacedOutLecturePeriodsShouldUpdate() {
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 1, 1)));
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 6, 1)));
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 10, 31)));
		this.courseSchedule.add(new LecturePeriod("Test1", "Testing", LocalDate.of(2016, 12, 31)));
		this.courseSchedule.updateAllDates(LocalDate.of(2017, 1, 1));
		assertEquals(this.courseSchedule.startDateProperty().getValue(), LocalDate.of(2017, 1, 1));
		assertEquals(this.courseSchedule.get(1).getDate().getValue(), LocalDate.of(2017, 1, 2));
		assertEquals(this.courseSchedule.get(2).getDate().getValue(), LocalDate.of(2017, 1, 3));
		assertEquals(this.courseSchedule.get(3).getDate().getValue(), LocalDate.of(2017, 1, 4));
	}

}
