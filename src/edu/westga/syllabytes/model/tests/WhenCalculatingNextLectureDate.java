package edu.westga.syllabytes.model.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import edu.westga.syllabytes.model.CourseSchedule;

public class WhenCalculatingNextLectureDate {
	
	private CourseSchedule courseSchedule;
	
	/**
	 * Initializes test CourseSchedule.
	 */
	@Before
	public void setup() {
		this.courseSchedule = new CourseSchedule();
	}
	
	/**
	 * Tests scenario where there is one lecture a week.
	 */
	@Test
	public void whenLectureIsOneDayAWeek() {
		this.courseSchedule.setMondayProperty(true);
		try {
			LocalDate nextClass = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 1));
			assertEquals(nextClass, LocalDate.of(2016, 8, 8));
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests scenario where there are two lectures a week in a row.
	 */
	@Test
	public void whenLectureIsTwoConsecutiveDaysAWeek() {
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setTuesdayProperty(true);
		try {
			LocalDate nextClass = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 1));
			assertEquals(nextClass, LocalDate.of(2016, 8, 2));
			LocalDate nextClass1 = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 2));
			assertEquals(nextClass1, LocalDate.of(2016, 8, 8));
			LocalDate nextClass2 = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 3));
			assertEquals(nextClass2, LocalDate.of(2016, 8, 8));
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests scenario where there are two lectures a week.
	 */
	@Test
	public void whenLectureIsTwoNonConsecutiveDaysAWeek() {
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setWednesdayProperty(true);
		try {
			LocalDate nextClass = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 1));
			assertEquals(nextClass, LocalDate.of(2016, 8, 3));
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests scenario where there are two lectures a week far apart in the week.
	 */
	@Test
	public void whenLectureIsTwoNonConsecutiveDaysAWeekThatAreFarApart() {
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setSaturdayProperty(true);
		try {
			LocalDate nextClass = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 1));
			assertEquals(nextClass, LocalDate.of(2016, 8, 6));
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests scenario where there are no boolean day properties set in the courseschedule
	 * @throws Exception
	 */
	@Test(expected = Exception.class)
	public void whenThereAreNoLecturesExceptionIsThrown() throws Exception {
		LocalDate nextClass = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 1));
		assertEquals(nextClass, LocalDate.of(2016, 8, 6));
	}
	
	/**
	 * Tests scenario where there are lectures every day of the week.
	 */
	@Test
	public void whenLectureIsEveryDayOfTheWeek() {
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setTuesdayProperty(true);
		this.courseSchedule.setWednesdayProperty(true);
		this.courseSchedule.setThursdayProperty(true);
		this.courseSchedule.setFridayProperty(true);
		this.courseSchedule.setSaturdayProperty(true);
		this.courseSchedule.setSundayProperty(true);
		try {
			LocalDate nextClass = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 1));
			assertEquals(nextClass, LocalDate.of(2016, 8, 2));
			LocalDate nextClass1 = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 2));
			assertEquals(nextClass1, LocalDate.of(2016, 8, 3));
			LocalDate nextClass2 = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 3));
			assertEquals(nextClass2, LocalDate.of(2016, 8, 4));
			LocalDate nextClass3 = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 4));
			assertEquals(nextClass3, LocalDate.of(2016, 8, 5));
			LocalDate nextClass4 = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 5));
			assertEquals(nextClass4, LocalDate.of(2016, 8, 6));
			LocalDate nextClass5 = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 6));
			assertEquals(nextClass5, LocalDate.of(2016, 8, 7));
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests that the calcNextScheduleDate method can find the next day if the given day isn't a lecture day
	 */
	@Test
	public void findNextLectureDayGivenANonLectureDay() {
		this.courseSchedule.setMondayProperty(true);
		try {
			LocalDate nextClass = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 2));
			assertEquals(nextClass, LocalDate.of(2016, 8, 8));
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests scenario where there is one lecture a week.
	 * @throws Exception 
	 */
	@Test(expected = Exception.class)
	public void whenLectureIsSetThenUnsetExceptionShouldBeThrown() throws Exception {
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setMondayProperty(false);
		LocalDate nextClass = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 1));
		assertEquals(nextClass, LocalDate.of(2016, 8, 8));
	}
	
	/**
	 * Tests that the calcNextScheduleDate method can find the next day if the given day is the same lecture day
	 */
	@Test
	public void findNextLectureDayGivenSameLectureDay() {
		this.courseSchedule.setMondayProperty(true);
		try {
			LocalDate nextClass = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 1));
			assertEquals(nextClass, LocalDate.of(2016, 8, 8));
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests that the calcNextScheduleDate method can find the next day if the given day is a next lecture day
	 */
	@Test
	public void findNextLectureDayGivenNextLectureDay() {
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setThursdayProperty(true);
		try {
			LocalDate nextClass = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 4));
			assertEquals(nextClass, LocalDate.of(2016, 8, 8));
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests scenario where there are lectures various days of the week.
	 */
	@Test
	public void whenLecturesAreVariousDaysOfTheWeek() {
		this.courseSchedule.setMondayProperty(true);
		this.courseSchedule.setWednesdayProperty(true);
		this.courseSchedule.setThursdayProperty(true);
		this.courseSchedule.setFridayProperty(true);
		this.courseSchedule.setSundayProperty(true);
		try {
			LocalDate nextClass = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 1));
			assertEquals(nextClass, LocalDate.of(2016, 8, 3));
			LocalDate nextClass1 = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 3));
			assertEquals(nextClass1, LocalDate.of(2016, 8, 4));
			LocalDate nextClass2 = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 4));
			assertEquals(nextClass2, LocalDate.of(2016, 8, 5));
			LocalDate nextClass3 = this.courseSchedule.calcNextScheduleDate(LocalDate.of(2016, 8, 5));
			assertEquals(nextClass3, LocalDate.of(2016, 8, 7));
		} catch (Exception e) {
			fail();
		}
	}

}
