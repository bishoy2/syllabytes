package edu.westga.syllabytes.model.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import edu.westga.syllabytes.model.CourseSchedule;
import edu.westga.syllabytes.model.LecturePeriod;
/**
 * Test class for CourseSchedule.loadFromFile()
 * @author Blake Ishoy
 * @version 1
 */
public class WhenBuildingCourseSchedulesFromICalendarFiles {
	
	private CourseSchedule courseSchedule;
	
	/**
	 * Initializes file for tests.
	 */
	@Before
	public void setup() {
		this.courseSchedule = new CourseSchedule();
	}
	
	/**
	 * Tests if courseSchedule has the right amount of lecturePeriods loaded from file.
	 */
	@Test
	public void courseScheduleSizeIsEqualToNumOfVEvents() {
		try {
			this.courseSchedule.loadFromFile(new File("src/test.ics"));
			assertEquals(this.courseSchedule.size(), 16);
		} catch (Exception e) {
			fail(this.courseSchedule.size() + "");
		}
	}
	
	/**
	 * Tests nonrecurring events for the correct title.
	 */
	@Test
	public void nonRecurringVEventsHaveExpectedTitle() {
		try {
			this.courseSchedule.loadFromFile(new File("src/test.ics"));
			LecturePeriod test = this.courseSchedule.get(15);
			assertEquals(test.getTitle().getValue(), "Joe's Birthday");
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests nonrecurring events for the correct description.
	 */
	@Test
	public void nonRecurringVEventsHaveExpectedDescription() {
		try {
			this.courseSchedule.loadFromFile(new File("src/test.ics"));
			LecturePeriod test = this.courseSchedule.get(15);
			assertEquals(test.getDescription().getValue(), "Joe is turning 40; his party is a surprise!");
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests nonrecurring events for the correct date.
	 */
	@Test
	public void nonRecurringVEventsHaveExpectedDate() {
		try {
			this.courseSchedule.loadFromFile(new File("src/test.ics"));
			LecturePeriod test = this.courseSchedule.get(15);
			assertEquals(test.getDate().getValue(), LocalDate.of(2013, 06, 14));
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests recurring events for the correct title.
	 */
	@Test
	public void recurringVEventsHaveExpectedTitle() {
		try {
			this.courseSchedule.loadFromFile(new File("src/test.ics"));
			LecturePeriod test = this.courseSchedule.get(0);
			assertEquals(test.getTitle().getValue(), "weekly staff meeting");
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests recurring events for the correct description.
	 */
	@Test
	public void recurringVEventsHaveExpectedDescription() {
		try {
			this.courseSchedule.loadFromFile(new File("src/test.ics"));
			LecturePeriod test = this.courseSchedule.get(0);
			assertEquals(test.getTitle().getValue(), "weekly staff meeting");
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests recurring events for the correct date.
	 */
	@Test
	public void recurringVEventsHaveExpectedDateMonth() {
		try {
			this.courseSchedule.loadFromFile(new File("src/test.ics"));
			LecturePeriod test = this.courseSchedule.get(0);
			assertEquals(test.getDate().getValue().getMonth().getValue(), 1);
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests recurring events for the correct date.
	 */
	@Test
	public void recurringVEventsHaveExpectedDateYear() {
		try {
			this.courseSchedule.loadFromFile(new File("src/test.ics"));
			LecturePeriod test = this.courseSchedule.get(0);
			assertEquals(test.getDate().getValue().getYear(), 115);
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests recurring events for the correct date.
	 */
	@Test
	public void recurringVEventsHaveExpectedDateDay() {
		try {
			this.courseSchedule.loadFromFile(new File("src/test.ics"));
			LecturePeriod test = this.courseSchedule.get(0);
			assertEquals(test.getDate().getValue().getDayOfMonth(), 4);
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests loading an invalid file.
	 * @throws Exception if there is a problem with the test call.
	 */
	@Test(expected = Exception.class)
	public void loadAnInvalidFileShouldThrowException() throws Exception {
		this.courseSchedule.loadFromFile(new File("src/fake.ics"));
	}
	
	/**
	 * Tests loading a null file.
	 * @throws Exception if there is a problem with the test call.
	 */
	@Test(expected = Exception.class)
	public void nullFileShouldThrowException() throws Exception {
		this.courseSchedule.loadFromFile(null);
	}
	
	/**
	 * Tests loading an empty file. 
	 */
	@Test
	public void fileWithNoEventsShouldHaveEmptyCourseSchedule() {
		try {
			this.courseSchedule.loadFromFile(new File("src/noevents.ics"));
			assertEquals(this.courseSchedule.size(), 0);
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests loading a file with one non-recurring event. 
	 */
	@Test
	public void fileWithOneNonRecurringEvent() {
		try {
			this.courseSchedule.loadFromFile(new File("src/oneNonRecurringEvent.ics"));
			assertEquals(this.courseSchedule.size(), 1);
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests loading a file with multiple non-recurring events. 
	 */
	@Test
	public void fileWithMultipleNonRecurringEvents() {
		try {
			this.courseSchedule.loadFromFile(new File("src/multipleNonRecurring.ics"));
			assertEquals(this.courseSchedule.size(), 3);
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests loading a file with one recurring event. 
	 */
	@Test
	public void fileWithOneRecurringEvent() {
		try {
			this.courseSchedule.loadFromFile(new File("src/oneRecurringEvent.ics"));
			assertEquals(this.courseSchedule.size(), 15);
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests loading a file with multiple recurring events. 
	 */
	@Test
	public void fileWithMultipleRecurringEvents() {
		try {
			this.courseSchedule.loadFromFile(new File("src/multipleRecurring.ics"));
			assertEquals(this.courseSchedule.size(), 45);
		} catch (Exception e) {
			fail();
		}
	}
	
	/**
	 * Tests loading a file with multiple recurring and non-recurring events. 
	 */
	@Test
	public void fileWithMultipleRecurringAndNonRecurringEvents() {
		try {
			this.courseSchedule.loadFromFile(new File("src/multipleRecurringAndNonRecurring.ics"));
			assertEquals(this.courseSchedule.size(), 48);
		} catch (Exception e) {
			fail();
		}
	}

}
