package edu.westga.syllabytes.model;

import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Iterator;

import edu.emory.mathcs.backport.java.util.Collections;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ModifiableObservableListBase;
import javafx.collections.ObservableList;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Date;
import net.fortuna.ical4j.model.DateList;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.Recur;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.parameter.Value;
import net.fortuna.ical4j.model.property.RRule;
/**
 * Class corresponding to ICalendar VCALENDAR objects
 * @author Blake Ishoy
 * @version 1
 */
public class CourseSchedule extends ModifiableObservableListBase<LecturePeriod> implements ICourseScheduleModel {
	
	private ObservableList<LecturePeriod> courseSchedule;
	private BooleanProperty sundayProperty;
	private BooleanProperty mondayProperty;
	private BooleanProperty tuesdayProperty;
	private BooleanProperty wednesdayProperty;
	private BooleanProperty thursdayProperty;
	private BooleanProperty fridayProperty;
	private BooleanProperty saturdayProperty;
	
	/**
	 * Constructor for the CourseSchedule class.
	 */
	public CourseSchedule() {
		this.courseSchedule = FXCollections.observableArrayList();
		
		this.sundayProperty = new SimpleBooleanProperty(false);
		this.mondayProperty = new SimpleBooleanProperty(false);
		this.tuesdayProperty = new SimpleBooleanProperty(false);
		this.wednesdayProperty = new SimpleBooleanProperty(false);
		this.thursdayProperty = new SimpleBooleanProperty(false);
		this.fridayProperty = new SimpleBooleanProperty(false);
		this.saturdayProperty = new SimpleBooleanProperty(false);
	}
	
	@Override
	public ObservableList<LecturePeriod> getSchedule() {
		return this.courseSchedule;
	}
	
	@Override
	protected void doAdd(int index, LecturePeriod element) {
		this.courseSchedule.add(index, element);
	}

	@Override
	protected LecturePeriod doRemove(int index) {
		return this.courseSchedule.remove(index);	
	}

	@Override
	protected LecturePeriod doSet(int index, LecturePeriod element) {
		return this.courseSchedule.set(index, element);
	}

	@Override
	public LecturePeriod get(int index) {
		return this.courseSchedule.get(index);
	}

	@Override
	public int size() {
		return this.courseSchedule.size();
	}
	
	@Override
	public void loadFromFile(File icalFile) throws Exception {
		try {
			FileInputStream fin = new FileInputStream(icalFile);
			CalendarBuilder builder = new CalendarBuilder();
			Calendar calendar = builder.build(fin);
			for (Iterator<?> current = calendar.getComponents("VEVENT").iterator(); current.hasNext();) {
			    VEvent component = (VEvent) current.next();
			    
			    String name = component.getSummary().getValue();			    
			    String description = component.getProperty("DESCRIPTION").getValue(); 
			    LocalDate date = component.getStartDate().getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			    
			    if (component.getProperty("RRULE") != null) {
			    	this.handleRecurringEvents(component, name, description);
			    } else {
		    		this.courseSchedule.add(new LecturePeriod(name, description, date));
			    }
			    Collections.sort(this.courseSchedule);
			}
		} catch (Exception e) {
			throw new Exception();
		}
	}
	
	@SuppressWarnings("deprecation")
	private void handleRecurringEvents(VEvent component, String name, String description) throws ParseException {
		RRule rrule = new RRule(component.getProperty("RRULE").getValue());
		Recur recur = rrule.getRecur();
		
		DateTime startDateTime = new DateTime(component.getStartDate().getDate());
		Date endDate = component.getEndDate().getDate();
		endDate.setMonth(endDate.getMonth() + 4);
		DateTime endDateTime = new DateTime(endDate);
		
		DateList recurringDates = recur.getDates(startDateTime, endDateTime, Value.DATE_TIME);

		for (int count = 0; count < recurringDates.size(); count++) {
			DateTime currentDate = (DateTime) recurringDates.get(count);
			LocalDate localDate = LocalDate.of(currentDate.getYear(), currentDate.getMonth() + 1, currentDate.getDay());
			LecturePeriod lecturePeriod = new LecturePeriod(name, description, localDate);
			this.courseSchedule.add(lecturePeriod);
		}
	}

	@Override
	public ObjectProperty<LocalDate> startDateProperty() {
		if (this.courseSchedule.size() > 0 && this.courseSchedule.get(0) != null) {
			return this.courseSchedule.get(0).getDate();
		} else {
			return null;
		}
	}
	
	/**
	 * Changes all start dates in CourseSchedule such that they will all occur 
	 * on the specified days of the week from the specified start date.
	 * @param startDate the desired start date.
	 */
	public void updateAllDates(LocalDate startDate) {
		if (this.courseSchedule.size() <= 0) {
			return;
		}
		if (startDate == null) {
			throw new IllegalArgumentException("startDate can not be null");
		}
		LocalDate nextDate = null;
		for (LecturePeriod period : this.courseSchedule) {
			if (period != null) {
				try {
					if (this.courseSchedule.get(0) == period) {
						period.setDate(startDate);
					} else {
						period.setDate(this.calcNextScheduleDate(nextDate));
					}
					nextDate = period.getDate().getValue();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public BooleanProperty sundayProperty() {
		return this.sundayProperty;
	}
	
	/**
	 * Sets the Sunday Property.
	 * @param newValue the new value for the property.
	 */
	public void setSundayProperty(boolean newValue) {
		this.sundayProperty.setValue(newValue);
	}

	@Override
	public BooleanProperty mondayProperty() {
		return this.mondayProperty;
	}
	
	/**
	 * Sets the Monday Property.
	 * @param newValue the new value for the property.
	 */
	public void setMondayProperty(boolean newValue) {
		this.mondayProperty.setValue(newValue);
	}

	@Override
	public BooleanProperty tuesdayProperty() {
		return this.tuesdayProperty;
	}
	
	/**
	 * Sets the Tuesday Property.
	 * @param newValue the new value for the property.
	 */
	public void setTuesdayProperty(boolean newValue) {
		this.tuesdayProperty.setValue(newValue);
	}

	@Override
	public BooleanProperty wednesdayProperty() {
		return this.wednesdayProperty;
	}
	
	/**
	 * Sets the Wednesday Property.
	 * @param newValue the new value for the property.
	 */
	public void setWednesdayProperty(boolean newValue) {
		this.wednesdayProperty.setValue(newValue);
	}

	@Override
	public BooleanProperty thursdayProperty() {
		return this.thursdayProperty;
	}
	
	/**
	 * Sets the Thursday Property.
	 * @param newValue the new value for the property.
	 */
	public void setThursdayProperty(boolean newValue) {
		this.thursdayProperty.setValue(newValue);
	}

	@Override
	public BooleanProperty fridayProperty() {
		return this.fridayProperty;
	}
	
	/**
	 * Sets the Friday Property.
	 * @param newValue the new value for the property.
	 */
	public void setFridayProperty(boolean newValue) {
		this.fridayProperty.setValue(newValue);
	}

	@Override
	public BooleanProperty saturdayProperty() {
		return this.saturdayProperty;
	}
	
	/**
	 * Sets the Saturday Property.
	 * @param newValue the new value for the property.
	 */
	public void setSaturdayProperty(boolean newValue) {
		this.saturdayProperty.setValue(newValue);
	}
	
	/**
	 * Given a LocalDate to start with, this method determines the first date on or after it that falls 
	 * on one of the days-of-the-week in which the course meets
	 * @param date the current date
	 * @return the next date in the schedule
	 * @throws Exception if all the day booleans are false
	 */
	public LocalDate calcNextScheduleDate(LocalDate date) throws Exception {
		LocalDate nextDate = null;
		boolean dateNotSet = true;
		int day = date.getDayOfMonth();
		int month = date.getMonthValue();
		int year = date.getYear();
		if (!this.mondayProperty.get() && !this.tuesdayProperty.get() && !this.wednesdayProperty.get() && !this.thursdayProperty.get() && !this.fridayProperty.get()
				&& !this.saturdayProperty.get() && !this.sundayProperty.get()) {
			throw new Exception("There are no days of the week set!");
		}
		while (dateNotSet) {
			
			LocalDate testDate = LocalDate.of(year, month, day);
			testDate = testDate.plusDays(1);
			
			if (this.mondayProperty.get() && testDate.getDayOfWeek() == DayOfWeek.MONDAY) {
				nextDate = testDate;
				dateNotSet = false;
			} else if (this.tuesdayProperty.get() && testDate.getDayOfWeek() == DayOfWeek.TUESDAY) {
				nextDate = testDate;
				dateNotSet = false;
			} else if (this.wednesdayProperty.get() && testDate.getDayOfWeek() == DayOfWeek.WEDNESDAY) {
				nextDate = testDate;
				dateNotSet = false;
			} else if (this.thursdayProperty.get() && testDate.getDayOfWeek() == DayOfWeek.THURSDAY) {
				nextDate = testDate;
				dateNotSet = false;
			} else if (this.fridayProperty.get() && testDate.getDayOfWeek() == DayOfWeek.FRIDAY) {
				nextDate = testDate;
				dateNotSet = false;
			} else if (this.saturdayProperty.get() && testDate.getDayOfWeek() == DayOfWeek.SATURDAY) {
				nextDate = testDate;
				dateNotSet = false;
			} else if (this.sundayProperty.get() && testDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
				nextDate = testDate;
				dateNotSet = false;
			}
			
		}
		
		return nextDate;
	}	
}
