package edu.westga.syllabytes.model;

import java.io.File;
import java.time.LocalDate;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;

/**
 * Interface for the CourseSchedule
 * @author Blake Ishoy
 * @version 1
 */
public interface ICourseScheduleModel {
	
	/**
	 * Start date property
	 * @return the start date property
	 */
	ObjectProperty<LocalDate> startDateProperty();
	
	/**
	 * Returns the courseSchedule.
	 * @return the CourseSchedule
	 */
	ObservableList<LecturePeriod> getSchedule();
	
	/**
	 * Loads a Calendar from an ics file and adds the VEvents to the getSchedule data member.
	 * @param icalFile the File being imported.
	 * @throws Exception if something is wrong, i.e. invalid file
	 */
	void loadFromFile(File icalFile) throws Exception;
	
	/**
	 * Sunday property.
	 * @return sunday property.
	 */
	BooleanProperty sundayProperty();
	
	/**
	 * Monday property.
	 * @return monday property.
	 */
	BooleanProperty mondayProperty();
	
	/**
	 * Tuesday property.
	 * @return tuesday property.
	 */
	BooleanProperty tuesdayProperty();
	
	/**
	 * Wednesday property.
	 * @return wednesday property.
	 */
	BooleanProperty wednesdayProperty();
	
	/**
	 * Thursday property.
	 * @return thursday property.
	 */
	BooleanProperty thursdayProperty();
	
	/**
	 * Friday property.
	 * @return friday property.
	 */
	BooleanProperty fridayProperty();
	
	/**
	 * Saturday property.
	 * @return saturday property.
	 */
	BooleanProperty saturdayProperty();
}
