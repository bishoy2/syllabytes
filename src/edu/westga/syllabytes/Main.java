package edu.westga.syllabytes;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Contains the code that runs the Syllabytes application.
 * @author Blake Ishoy
 * @version 1
 */
public class Main extends Application {
	
	
	/**
	 * Starts the JavaFX Syllabytes application.
	 * @param args The args required to run the app
	 */
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("SyllabytesMainView.fxml"));
		stage.setTitle("Syllabytes by Blake Ishoy");
		stage.setScene(new Scene(root, 600, 400));
		stage.show();
	}

}
