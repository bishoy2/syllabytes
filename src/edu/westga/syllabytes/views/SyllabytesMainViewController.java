package edu.westga.syllabytes.views;

import java.time.LocalDate;

import edu.westga.syllabytes.model.CourseSchedule;
import edu.westga.syllabytes.model.LecturePeriod;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * Controller for Syllabytes application
 * @author Blake Ishoy
 * @version 1
 */
public class SyllabytesMainViewController {

	@FXML 
	private TableView<LecturePeriod> scheduleTable;
	
	@FXML 
	private TableColumn<LecturePeriod, LocalDate> dateColumn;
	
	@FXML 
	private TableColumn<LecturePeriod, String> titleColumn;
	
	@FXML 
	private TableColumn<LecturePeriod, String> descriptionColumn;
	
	@FXML
	private DatePicker datePicker;
	
	@FXML
    private CheckBox sundayBox;

    @FXML
    private CheckBox saturdayBox;
	
	@FXML
    private CheckBox fridayBox;

    @FXML
    private CheckBox thursdayBox;

    @FXML
    private CheckBox wednesdayBox;
    
    @FXML
    private CheckBox tuesdayBox;
    
    @FXML
    private CheckBox mondayBox;
	
	private CourseSchedule courseSchedule;
	
	/**
	 * Initializes the controller.
	 */
	public void initialize() {
		this.courseSchedule = new CourseSchedule();
		
		this.dateColumn.setCellValueFactory(cellData -> cellData.getValue().getDate());
		this.titleColumn.setCellValueFactory(cellData -> cellData.getValue().getTitle());
		this.descriptionColumn.setCellValueFactory(cellData -> cellData.getValue().getDescription());
		
		this.populateTableWithFakeData();
	}
	
	/**
	 * Links the courseSchedule with the TableView.
	 */
	private void tableViewLoad() {
		this.scheduleTable.setItems(this.courseSchedule.getSchedule());
		this.datePicker.setValue(LocalDate.now());
	}
	
	/**
	 * Dummy data for TableView.
	 */
	private void populateTableWithFakeData() {
		LecturePeriod newPeriod1 = new LecturePeriod("Class1", "Lecture 1", LocalDate.now());
		LecturePeriod newPeriod2 = new LecturePeriod("Class2", "Lecture 2", LocalDate.now());
		LecturePeriod newPeriod3 = new LecturePeriod("Class3", "Lecture 3", LocalDate.now());
		LecturePeriod newPeriod4 = new LecturePeriod("Class4", "Lecture 4", LocalDate.now());
		LecturePeriod newPeriod5 = new LecturePeriod("Class5", "Lecture 5", LocalDate.now());
		LecturePeriod newPeriod6 = new LecturePeriod("Class6", "Lecture 6", LocalDate.now());
		LecturePeriod newPeriod7 = new LecturePeriod("Class7", "Lecture 7", LocalDate.now());
		LecturePeriod newPeriod8 = new LecturePeriod("Class8", "Lecture 8", LocalDate.now());
		LecturePeriod newPeriod9 = new LecturePeriod("Class9", "Lecture 9", LocalDate.now());
		this.courseSchedule.add(newPeriod1);
		this.courseSchedule.add(newPeriod2);
		this.courseSchedule.add(newPeriod3);
		this.courseSchedule.add(newPeriod4);
		this.courseSchedule.add(newPeriod5);
		this.courseSchedule.add(newPeriod6);
		this.courseSchedule.add(newPeriod7);
		this.courseSchedule.add(newPeriod8);
		this.courseSchedule.add(newPeriod9);
		this.tableViewLoad();
	}
	
	@FXML
    private void pickDate(ActionEvent event) {
		if (!this.mondayBox.isSelected() && !this.tuesdayBox.isSelected() && !this.wednesdayBox.isSelected() && !this.thursdayBox.isSelected() && 
				!this.thursdayBox.isSelected() && !this.fridayBox.isSelected() && !this.saturdayBox.isSelected() && !this.sundayBox.isSelected()) {
			new Alert(AlertType.ERROR, "Please select a day of the week checkbox.", ButtonType.CLOSE).show();
		} else {
			this.courseSchedule.updateAllDates(this.datePicker.getValue());
		}
    }
	
	@FXML
    private void onMondayBoxChecked(ActionEvent event) throws Exception {
		if (this.mondayBox.isSelected()) {
			this.courseSchedule.setMondayProperty(true);
		} else {
			this.courseSchedule.setMondayProperty(false);
		}
		this.courseSchedule.updateAllDates(this.courseSchedule.calcNextScheduleDate(this.datePicker.getValue()));
    }
	
	@FXML
	private void onTuesdayBoxChecked(ActionEvent event) throws Exception {
		if (this.tuesdayBox.isSelected()) {
			this.courseSchedule.setTuesdayProperty(true);
		} else {
			this.courseSchedule.setTuesdayProperty(false);
		}
		this.courseSchedule.updateAllDates(this.courseSchedule.calcNextScheduleDate(this.datePicker.getValue()));
    }

    @FXML
    private void onWednesdayBoxChecked(ActionEvent event) throws Exception {
    	if (this.wednesdayBox.isSelected()) {
			this.courseSchedule.setWednesdayProperty(true);
		} else {
			this.courseSchedule.setWednesdayProperty(false);
		}
    	this.courseSchedule.updateAllDates(this.courseSchedule.calcNextScheduleDate(this.datePicker.getValue()));
    }

    @FXML
    private void onThursdayBoxChecked(ActionEvent event) throws Exception {
    	if (this.thursdayBox.isSelected()) {
			this.courseSchedule.setThursdayProperty(true);
		} else {
			this.courseSchedule.setThursdayProperty(false);
		}
    	this.courseSchedule.updateAllDates(this.courseSchedule.calcNextScheduleDate(this.datePicker.getValue()));
    }

    @FXML
    private void onFridayBoxChecked(ActionEvent event) throws Exception {
    	if (this.fridayBox.isSelected()) {
			this.courseSchedule.setFridayProperty(true);
		} else {
			this.courseSchedule.setFridayProperty(false);
		}
    	this.courseSchedule.updateAllDates(this.courseSchedule.calcNextScheduleDate(this.datePicker.getValue()));
    }

    @FXML
    private void onSaturdayBoxChecked(ActionEvent event) throws Exception {
    	if (this.saturdayBox.isSelected()) {
			this.courseSchedule.setSaturdayProperty(true);
		} else {
			this.courseSchedule.setSaturdayProperty(false);
		}
    	this.courseSchedule.updateAllDates(this.courseSchedule.calcNextScheduleDate(this.datePicker.getValue()));
    }

    @FXML
    private void onSundayBoxChecked(ActionEvent event) throws Exception {
    	if (this.sundayBox.isSelected()) {
			this.courseSchedule.setSundayProperty(true);
		} else {
			this.courseSchedule.setSundayProperty(false);
		}
    	this.courseSchedule.updateAllDates(this.courseSchedule.calcNextScheduleDate(this.datePicker.getValue()));
    }
	
}
